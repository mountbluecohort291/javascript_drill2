# Javascript_Drill2

1. This drill is part of javascript Drills given by Gangadhar sir.
2. This project has 8 Problem sets and their respective test sets.
3. Each problem set has it's own Commit.
4. main is the default branch.

## Languages used

Javascript, Nodes.js, Npm

```bash

npm init
```
## Structure
- Test problems
- Data set (data.js)
- Test problems in test Directory.
## Instructions
1.The project uses Es6 Module system.
2. All the code are run in Node.js environment.
## Commands

```Node.js
--Initialize The Project--
npm init

# --To Run And Test Problems--
node ./test/<testproblemset>.js

```
## License
[MIT](https://choosealicense.com/licenses/mit/)

## Author
Copyright (c) 2024, VISHAL ANTHONY.