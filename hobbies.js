export function hobbies(dataSet) {
  //exports to ./test/testHobbies.js
  if (!Array.isArray(dataSet) || dataSet.length == []) {
    return "Error:422 Invalid Input!";
  }
  const personOver30 = []; // To store person at specific age 30
  for (let data of dataSet) {
    if (data.age == 30) {
      personOver30.push({
        name: data.name,
        age: data.age,
        hobbies: data.hobbies,
      });
    }
  }
  let hobbyList = ""; // stores the hobbies array elements
  if (personOver30.length != 0) {
    //check if person at age 30 exists in data.
    for (let person of personOver30) {
      hobbyList += `${person.name} is ${person.age} years old and have hobbies: \n`;
      for (let hobby of person["hobbies"]) {
        hobbyList += `${hobby} \n`;
      }
    }
  } else {
    hobbyList += "There are no person over 30 in the data.";
  }
  return hobbyList;
}
