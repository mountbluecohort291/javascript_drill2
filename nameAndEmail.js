export function nameAndEmail(dataSet) {
  //exports to ./test/testNameandEmail.js
  if (!Array.isArray(dataSet) || dataSet.length == 0) {
    return "Error:422 Invalid Input!";
  }
  let nameAndEmailStore = []; //Store name and email of person of age 25
  let nameAndEmail = "The Persons Of Age 25:\n";
  for (let data of dataSet) {
    if (data.age == 25) {
      nameAndEmailStore.push({ name: data.name, age: data.age });
    }
  }
  for (let bio of nameAndEmailStore) {
    nameAndEmail += `Name:${bio.name}, Age:${bio.age}`;
  }

  return nameAndEmail;
}
