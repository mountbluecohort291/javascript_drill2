export function position3Student(studentData) {
  //exports to ./test/testPosition3Student.js
  if (!Array.isArray(studentData) || studentData.length == 0) {
    return "Error:422 Invalid Input";
  }
  return `Name: ${studentData[3]["name"]}, City: ${studentData[3]["city"]}`;
}
