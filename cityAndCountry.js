export function cityAndCountry(dataSet) {
  //exports to ./test/testCityCountry.js
  if (!Array.isArray(dataSet) || dataSet.length == []) {
    return "Error:422 Invalid Input!";
  }
  let cityAndCountry = "";
  for (let data of dataSet) {
    cityAndCountry += `NAME: ${data["name"]}, CITY: ${data["city"]}, COUNTRY: ${data["country"]}\n`;
  }
  return cityAndCountry;
}
