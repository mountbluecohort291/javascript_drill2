//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.

export function emailAddress(dataSet) {
  //exports to ./test/test_emailAddress.js

  if (!Array.isArray(dataSet) || dataSet.length == 0) {
    return "Error:422 Invalid Input!";
  }
  let emailAddressList = "";
  let index = 1;
  for (let bioData of dataSet) {
    emailAddressList += `${index}): ${bioData["email"]}\n`;
    index++;
  }
  return `Email Address List: \n${emailAddressList}`;
}
