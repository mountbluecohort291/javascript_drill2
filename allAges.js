export function allAges(dataSet) {
  //exports to ./test/testAllAges.js
  if (!Array.isArray(dataSet) || dataSet.length == 0) {
    return "Error:422 Invalid Input!";
  }
  let ageList = ""; //To store all the ages of people
  for (let data of dataSet) {
    ageList += `-> ${data.age}\n`;
  }
  return `The Ages Of all People:\n${ageList}`;
}
