export function firstHobby(dataSet) {
  //exports to ./test/testFirstHobby.js
  if (!Array.isArray(dataSet) || dataSet.length == 0) {
    return "Error:422 Invalid Input!";
  }
  let firstHobbyStore = []; //to store the hobbies in an array
  let firstHobbyList = "The First Hobby Of Each Person is:\n";
  for (let data of dataSet) {
    firstHobbyStore.push(data["hobbies"][0]);
  }
  for (let hobby of firstHobbyStore) {
    firstHobbyList += `->${hobby}\n`;
  }
  return firstHobbyList;
}
