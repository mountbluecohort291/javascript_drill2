export function australianStudents(dataSet) {
  //exports to ./test/testAustralianStudents.js
  let australianStudents = [];
  for (let studentData of dataSet) {
    if (studentData.isStudent && studentData.country == "Australia") {
      australianStudents.push(studentData.name);
    }
  }
  let australianStudentNames = "Names of Students From Australia: \n";
  if (!australianStudents.length == 0) {
    for (let name of australianStudents) {
      australianStudentNames += `${name}\n`;
    }
  }
  return australianStudentNames;
}
